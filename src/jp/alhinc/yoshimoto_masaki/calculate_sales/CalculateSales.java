package jp.alhinc.yoshimoto_masaki.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {

    public static void main(String[] args){


    	Map<String, String> storeInfo = new HashMap<String, String>();//店舗情報
    	Map<String, Long> salesInfo = new HashMap<String, Long>();//売上情報
    	List<Integer> numberList = new ArrayList<Integer>();//売上ファイル連番チェック用




    	BufferedReader br = null;
    	File directory = new File(args[0]);
    	String[] fileName = directory.list();
    	if(Arrays.asList(fileName).contains("branch.lst")){//支店定義ファイルの有無を確認
    		try{
    			File file = new File(args[0], "branch.lst");
    			FileReader fr = new FileReader(file);
    			br = new BufferedReader(fr);
    			String fr1;
    			while((fr1 = br.readLine()) != null){
    				String[] storeCode = fr1.split(",", -1);
    				if(storeCode[0].matches("^[0-9]{3}")){//支店コードが数字３桁のでtrue
    					if(storeCode.length == 2){//ファイルの要素数が2でture
    						if(storeCode[1].endsWith(",") != true){//支店名末尾が，のときfals
    							if((storeCode[1].replaceAll("　", "").replaceAll(" ", "").equals("") != true)){
    								storeInfo.put(storeCode[0], storeCode[1]);//支店マップに書き込み
    								salesInfo.put(storeCode[0], (long) 0);//売上マップに書き込み
    							}else{
    								System.out.println("支店定義ファイルのフォーマットが不正です");
    								return;
    							}
    						}else{
    							System.out.println("支店定義ファイルのフォーマットが不正です");
    							return;
    						}
    					}else{
    						System.out.println("支店定義ファイルのフォーマットが不正です");
    						return;
    					}
    				}else{
    					System.out.println("支店定義ファイルのフォーマットが不正です");
    					return;
    				}
    			}
    		}catch(IOException e){
    			System.out.println("予期せぬエラーが発生しました");
    			return;
    		}finally{
    			try{
    				br.close();
    			}catch(IOException e){
    				System.out.println("予期せぬエラーが発生しました");
    				return;
    			}
    		}
    	}else{
    		System.out.println("支店定義ファイルが存在しません");
    		return;
    	}





    	File directory1 = new File(args[0]);
    	BufferedReader br2 = null;
    	for(String filename : directory1.list()){
    		if(filename.matches("^\\d{8}.rcd$")){
    			String[] filename2 = filename.split("\\.");
    			Integer fileNumbers = Integer.parseInt(filename2[0]);//Integer型に変換
    			numberList.add(fileNumbers);//連番チェックリストに送ります
    			try {
    				br2 = new BufferedReader(new FileReader(new File(args[0], filename)));
    				String salesCode = br2.readLine();
    				List<String> storeList = new ArrayList<>(storeInfo.keySet());
    				if(storeList.contains(salesCode)){//支店の該当チェック
    					Long salesNumber = Long.parseLong(br2.readLine());
    					if(br2.readLine() == null){//売上ファイル3桁目チェック
    						Long salesCode2 = salesInfo.get(salesCode);
    						Long salesTotal = salesCode2 + salesNumber;//コードごとに合計
    						String sortName = String.valueOf(salesTotal);//文字列変換
    						if(sortName.matches("^\\d{1,10}")){//合計金額10桁か
    							salesInfo.put(salesCode, salesTotal);//売上マップに書き込み
    						}else{
    							System.out.println("合計金額が10桁を超えました");
    							return;
    						}
    					}else{
    						System.out.println(filename + "のフォーマットが不正です");
    						return;
    					}
    				}else{
    					System.out.println(filename + "の支店コードが不正です");
    					return;
    				}
    			}catch(IOException e){
    				System.out.println("予期せぬエラーが発生しました");
    				return;
    			}finally{
    				if(br2 != null){
    					try{
    						br2.close();
    					}catch(IOException e){
    						System.out.println("予期せぬエラーが発生しました");
    						return;
    					}
    				}else{
    					System.out.println("予期せぬエラーが発生しました");
    					return;
    				}
    			}
    		}
    	}




    	Collections.sort(numberList);//連番チェック昇順にソート
    	if(numberList.size() == 0 ){//ファイルが無いときにはエラー
    		System.out.println("予期せぬエラーが発生しました");
    		return;
    	}else{
    		if(numberList.get(0) + numberList.size() -1 == numberList.get(numberList.size() -1) ){
    		}else{
    			System.out.println("売上ファイル名が連番になっていません");
    			return;
    		}
    	}




    	File file = new File(args[0], "branch.out");
    	FileWriter fw = null;
    	try{
    		fw = new FileWriter(file);
    		for(Map.Entry<String, String> entry : storeInfo.entrySet()){
    			String salesCode = entry.getKey();
    			String name = entry.getValue();
    			Long salesTotal = salesInfo.get(salesCode);
    			fw.write(salesCode + "," +name + "," + salesTotal + "\r\n");
    		}
    	}catch(IOException e){
    		System.out.println("予期せぬエラーが発生しました");
    		return;
    	}finally{
    		try{
    			fw.close();
    		}catch(IOException e){
    			System.out.println("予期せぬエラーが発生しました");
    			return;
    		}
    	}


    }
}